DiceGen
===
A [Diceware](http://world.std.com/~reinhold/diceware.html) password generator in shell script.

Features
---
* Designed with simplicity in mind
* POSIX compatible
* Flexible -- Easy to use custom wordlists
* Also can be used to generate random ASCII passwords
* Able to insert characters at random positions for added entropy

Dependencies
---
* POSIX compatible shell
* Common shell utilities (`od`, `wc`, `cut`, `tail` and `head` for the core script)
* `/dev/random` block device

In other words, most Linux/BSD/OSX/*nix systems should be fine.

Verifying
---
Since the scripts are quite short, it is advised that you manually go through them to check that there's no vulnerabilities or backdoors. In additition, although the wordlists are too long to be reviewed by hand, one can use the following commands:
```
# Check if the words are unique, output should be "[Wordlist length] 1"
cat wordlist.txt | sort -n | uniq -c | sed -e 's/.*\([0-9]\+\).*/\1/g' | uniq -c
# Check if the initials are as described in this document (i.e. prepended with slashes, capitalized, ...etc)
cat wordlist.txt | sed -e 's/\(.\).*/\1/g' | sort -n | uniq -c
```

Usage
---
### `./diceGen.sh [Length] [Wordlist]`
Generates a diceware password using `[Length]` words from the `[Wordlist]` file.

### `./genChars.sh [Arguments]`
A helper script that generates a wordlist based on characters. `[Arguments]` are passed to `tr`, which filters through all the printable ASCII characters. This is useful for generating random ASCII passwords.

Requires `tr` to be installed.

### `./rotate.sh [Template] [Wordlists...]`
A helper script that calls `./dicegen.sh` multiple times to generate a password based on different wordlists. `[Template]` is a sequence of numbers determining how the wordlists should be used. Specifically, if `k` is the i-th number in the sequence, then for the i-th part of the password, the `k - 1`-th file in `[Wordlists]` is used.

Requires `sed` to be installed.

### `./randChar.sh [CharCount] [Wordlist]`
A helper script that inserts `[CharCount]` random words from `[Wordlist]` in random positions of the password being passed via stdin.

Requires `sed` to be installed.

Wordlists
---
The wordlist format is fairly simple, consisting of each word on its own line.

### Included Wordlists (in `wordlists/`)
* `diceware-wordlist.txt` -- [The original Diceware list](http://world.std.com/~reinhold/diceware.html) with a slash prepended to each word
* `diceware8k.txt` -- [The 8k wordlist](http://world.std.com/~reinhold/diceware8k.txt) with a slash appended to each word
* `eff-wordlist.txt` -- [The EFF wordlist](https://www.eff.org/deeplinks/2016/07/new-wordlists-random-passphrases) with the first letters capitalized
* `ascii.txt` -- All printable ASCII characters
* `pronounce.txt` -- Permutations of CV to create pronounceable passwords

The capitalization and prepending slashes are meant to avoid situations like `9 9` and `99`. Without the separators, the two would appear the same, therefore reducing the entropy.

Examples
---
### Generate a Diceware password of 6 words from the original wordlist
```
./diceGen.sh 6 wordlists/diceware-wordlist.txt
```

### Generate a 10-character ASCII password
```
./diceGen.sh 10 wordlists/ascii.txt
```

### Generate a 20-character alphabetic password
```
./genChars.sh -cd '[:alpha:]' > alpha.txt
./diceGen.sh 20 alpha.txt
```

### Generate a 6-word Diceware password with 5 ASCII characters appended
```
./rotate.sh 22222233333 wordlist/diceware-wordlist.txt wordlists/ascii.txt
```

### Generate a 6-word Diceware password, with 2 random characters inserted at random positions
```
./diceGen.sh 6 wordlists/diceware-wordlist.txt | ./randChar.sh 2 wordlists/ascii.txt
```

Randomness
---
The script uses `od` to get 2 bytes from `/dev/random`, creating an integer with a range of [0, 65536). It is then truncated to the length of the wordlist by doing a simple modulus operation, re-fetching the number if it is big enough to incur a bias. See [randLib.sh](randLib.sh) for more details.

Known Issues
---
* Can not handle wordlists longer than 65536 words -- this is due to the RNG only fetching 2 bytes
* `./rotate.sh` can not handle more than 8 wordlists -- numbers can only be ranged 2 ~ 9

